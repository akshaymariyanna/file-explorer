import React from "react";
import styled from "styled-components";

export const ButtonPanel = styled.div`
  display: flex;
  justify-content: flex-end;
`

export const MyButton = styled.button.attrs({
  className: "material-icons"
})`
  display: flex;
  padding: 0;
  margin: 5px;
  background-color: transparent;
  color: ${props => props.disabled ? "gray" : "lightcyan"};
  &:hover {
    color: ${props => props.disabled ? "gray" : "lightsalmon" };
  }
  outline: none;
  border: none;
  cursor: ${props => props.disabled ? "default" : "pointer" };
`

export default function FolderActions(props)  {
  return (
    <ButtonPanel>
      <MyButton
        onClick={props.addFolder}
      >
        create_new_folder
      </MyButton>
      <MyButton
        onClick={props.addFile}
      >
        add
      </MyButton>
      {
        props.showDelete && (
          <MyButton
            disabled={props.disableDel}
            onClick={props.delete}
          >
            delete
          </MyButton>
        )
      }
    </ButtonPanel>
  );
}
