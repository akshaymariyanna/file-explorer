import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

class Tracker extends React.Component {
  render() { return null; }

  componentDidUpdate(prevProps) {
    if (prevProps.short !== this.props.short) {
      localStorage.setItem("short", JSON.stringify(this.props.short));
    }
    if (prevProps.long !== this.props.long) {
      localStorage.setItem("long", JSON.stringify(this.props.long));
    }
  }
}

Tracker.propTypes = {
  short: PropTypes.array,
  long: PropTypes.array,
}

function mapStateToProps(store, ownProps) {
  return {
    short: store.short.blocks,
    long: store.long.blocks,
  };
}

export default connect(mapStateToProps)(Tracker);
