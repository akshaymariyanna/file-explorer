import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import {Icon} from "./Node";

const FileDiv = styled.div`
  cursor: pointer;
  display: flex;
`
const FileName = styled.p`
  margin-left: 5px;
  color: ${props => props.isSelected ? "lightseagreen" : "darkslategray"};
  flex: 1;
`

export default class File extends React.PureComponent {
  render() {
    return (
      <FileDiv
        onClick={this.props.onClick}
      >
        <Icon>attachment</Icon>
        <FileName
          isSelected={this.props.isSelected}
        >
          {this.props.name}
        </FileName>
      </FileDiv>
    )
  }
}

File.propTypes = {
  block: PropTypes.number.isRequired,
  tree: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  parent: PropTypes.number,
  onClick: PropTypes.func,
  isSelected: PropTypes.bool,
};
