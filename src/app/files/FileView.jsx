import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import styled from "styled-components";

import Node, {LastModified, Format} from "./Node";
import FolderActions, {ButtonPanel, MyButton} from "../FolderActions";

import {addNewFile, addNewFolder,setCurrentFolder, deleteNode, restoreNode, cleanBin} from "../../lib/reducers";

const FileHolder = styled.div`
  display: flex;
  flex-direction: column;
  ${ButtonPanel} {
    background-color: darkslategray;
    padding: 5px 0;
  }
  max-height: 100%;
`

export const Row = styled.div`
  display: flex;
  margin: 10px 0;
`

export const FileNameHeading = styled.p`
  flex: 1;
  text-overflow: ellipsis;
  margin: 0 30px;

  line-height: 14px;
  font-size: 14px;
`

const FileSpace = styled.div`
  padding: 10px;
  flex-direction: column;
  display: flex;
  flex: 1;
  overflow: auto;
`

class FileView extends React.PureComponent {
  componentDidMount() {
    if (this.props.type !== 1) {
      this.props.setCurrentFolder(this.props.parent, this.props.tree);
    }
  }

  render() {
    return (
      <FileHolder>
        { this.props.isRecycleView ? (
          <ButtonPanel>
            <MyButton
              disabled={this.props.disableDel}
              onClick={this.restore}
            >
              restore
            </MyButton>
            <MyButton
              disabled={this.props.nodes.length <= 0}
              onClick={this.clean}
            >
              clear
            </MyButton>
          </ButtonPanel>
        ) : (
          <FolderActions
            addFile={this.addFile}
            addFolder={this.addFolder}
            delete={this.delete}
            showDelete={true}
            disableDel={this.props.disableDel}
          />
        )}
        <FileSpace>

          <Row>
            <FileNameHeading>Name</FileNameHeading>
            <LastModified>Last Modified</LastModified>
            <Format>Format</Format>
          </Row>
          {
            this.props.nodes && this.props.nodes.map((x) => (
              <Node
                key={`node-${x}`}
                block={x}
                tree={this.props.tree}
              />
            ))
          }
        </FileSpace>
      </FileHolder>
    )
  }

  addFolder = () => {
    this.props.addNewFolder(this.props.block, this.props.tree, "New Folder");
  }

  addFile = () => {
    this.props.addNewFile(this.props.block, this.props.tree, "New File");
  }

  delete = () => {
    this.props.deleteNode(this.props.tree, this.props.selected);
  }

  restore = () => {
    this.props.restoreNode(this.props.tree, this.props.selected);
  }

  clean = () => {
    this.props.cleanBin(this.props.tree);
  }
}

FileView.propTypes = {
  block: PropTypes.number.isRequired,
  tree: PropTypes.string.isRequired,
  type: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  nodes: PropTypes.arrayOf(PropTypes.number),
  parent: PropTypes.number,
  disableDel: PropTypes.bool,
  isRecycleView: PropTypes.bool,
  selected: PropTypes.number,
}

function mapStateToProps(store, ownProps) {
  const { type, name, nodes, parent } = store[ownProps.tree].blocks[ownProps.block];
  return {
    name,
    nodes,
    parent,
    type,
    selected: store[ownProps.tree].currentNode,
    disableDel: store[ownProps.tree].currentNode === ownProps.block,
    isRecycleView: ownProps.block === 1,
  }
}

const mapDispatchToProps = {
  addNewFile,
  addNewFolder,
  setCurrentFolder,
  deleteNode,
  restoreNode,
  cleanBin,
}

export default connect(mapStateToProps, mapDispatchToProps)(FileView);
