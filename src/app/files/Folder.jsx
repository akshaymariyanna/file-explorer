import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import {Icon} from "./Node";

const FolderContent = styled.div`
  cursor: default;
  display: flex;
`

const FolderName = styled.p`
  margin-left: 5px;
  color: ${props => props.isSelected ? "lightseagreen" : "darkslategray"};
  flex: 1;
`

export default class Folder extends React.PureComponent {
  render() {
    return (
      <FolderContent
        onClick={this.props.onClick}
        onDoubleClick={this.props.onDoubleClick}
      >
        <Icon>folder</Icon>
        <FolderName
          isSelected={this.props.isSelected}
        >
          {this.props.name}
        </FolderName>
      </FolderContent>
    )
  }
}

Folder.propTypes = {
  block: PropTypes.number.isRequired,
  tree: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  parent: PropTypes.number,
  onClick: PropTypes.func,
  onDoubleClick: PropTypes.func,
  isSelected: PropTypes.bool,
};
