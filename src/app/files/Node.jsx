import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import styled from "styled-components";

import Folder from "./Folder";
import File from "./File";

import {setCurrentFolder, setCurrentNode} from "../../lib/reducers";

export const Icon = styled.span.attrs({
  className: "material-icons",
})`
  background-color: transparent;
  margin: auto;
`

export const Row = styled.div`
  display: flex;
`

export const FileName = styled.div`
  flex: 1;
  text-overflow: ellipsis;
  margin: 0 10px;

  line-height: 14px;
  font-size: 14px;
`
export const LastModified = styled.p`
  margin: auto 10px;
  min-width: 150px;
  padding: 0;
  line-height: 14px;
  font-size: 14px;
  text-align: center;
`

export const Format = styled.p`
  text-align: center;
  margin: auto 10px;
  min-width: 150px;
  line-height: 14px;
  font-size: 14px;
`


class Node extends React.PureComponent {
  render() {
    const { block, tree, type, name, parent, isSelected, lastModified, format } = this.props;
    return (
      <Row>
        <FileName>
          {
            type === 1 ? (
              <Folder
                block={block}
                tree={tree}
                name={name}
                parent={parent}
                onClick={this.fileClick}
                onDoubleClick={this.folderClick}
                isSelected={isSelected}
              />
            ) : (
              <File
                block={block}
                tree={tree}
                name={name}
                onClick={this.fileClick}
                isSelected={isSelected}
              />
            )
          }
        </FileName>
        <LastModified>{lastModified}</LastModified>
        <Format>{format}</Format>
      </Row>
    );
  }

  folderClick = () => {
    this.props.setCurrentFolder(this.props.block, this.props.tree);
    this.props.setCurrentNode(this.props.block, this.props.tree);
  }

  fileClick = () => {
    this.props.setCurrentNode(this.props.block, this.props.tree);
  }
}

Node.propTypes = {
  block: PropTypes.number.isRequired,
  tree: PropTypes.string.isRequired,
  type: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  parent: PropTypes.number,
  isSelected: PropTypes.bool,
};

function mapStateToProps(store, ownProps) {
  const { type, name, parent, lastModified,format } = store[ownProps.tree].blocks[ownProps.block];
  return {
    name,
    parent,
    type,
    lastModified,
    format,
    isSelected: ownProps.block === store[ownProps.tree].currentNode,
  }
}

const mapDispatchToProps = {
  setCurrentFolder,
  setCurrentNode,
}

export default connect(mapStateToProps, mapDispatchToProps)(Node);
