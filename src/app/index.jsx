import React from 'react';
import {connect} from "react-redux";
import styled from "styled-components";
import PropTypes from "prop-types";

import Tracker from "./Tracker";
import TreeNode from "./tree/Node";
import FileView from "./files/FileView";

const AppDiv = styled.div`
  display: flex;
  height: 100%;
`

const TreeViewDiv = styled.div`
  flex: 2;
  display: flex;
  flex-direction: column;
  background-color: darkslategray;
`

const FileViewDiv = styled.div`
  flex: 4;
  background-color: cream;
`

class App extends React.PureComponent {
  render() {
    const {current, currentFolder} = this.props;
    return (
      <AppDiv>
        <TreeViewDiv>
          <TreeNode block={0} tree={current} key={`root-${current}`} special="root"/>
          <TreeNode block={1} tree={current} key={`recycle-${current}`} special="recycle"/>
        </TreeViewDiv>
        <FileViewDiv>
          <FileView tree={current} block={currentFolder}/>
        </FileViewDiv>
        <Tracker />
      </AppDiv>
    );
  }
}

App.propTypes = {
  current: PropTypes.string,
  currentFolder: PropTypes.number,
}

export default connect((store) => ({
  current: store.current,
  currentFolder: store[store.current].currentFolder,
  currentNode: store[store.current].currentNode,
}))(App);
