import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

import {MyButton} from "../FolderActions";

const BinDiv = styled.div`
  padding: 5px;
  display: flex;
  flex-direction: column;
`

const ButtonText = styled.span`
  font-family: "Open Sans";
  font-size: 14px;
  line-height: 14px;
  flex: 1;
  margin: auto;
  margin-left: 10px;
`

const BinPanel = styled.div`
  display: flex;
  flex-direction: column;
  margin: auto;
  min-height: 50px;
  flex: 1;
  ${MyButton} {
    line-height: 14px;
    display: inline-flex;
    margin: auto;
    padding: 0;
  }
`

export default class Bin extends React.PureComponent {
  render() {
    return (
      <BinDiv>
        <BinPanel>
          <MyButton onClick={this.props.openRecycleBin}>
            delete
            <ButtonText>Recycle Bin</ButtonText>
          </MyButton>
        </BinPanel>
      </BinDiv>
    )
  }
}

Bin.propTypes = {
  openRecycleBin: PropTypes.func,
};
