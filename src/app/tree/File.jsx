import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import {Icon} from "./Folder";

const FileDiv = styled.div`
  color: ${props => props.isCurrent ? "lightseagreen" : "lightcyan"};
  cursor: pointer;
  display: flex;
  flex-direction: row;
  ${Icon} {
    margin-left: 5px;
    line-height: 14px;
  }
  padding: 5px;
`

const FileText = styled.div`
  flex: 1;
  margin-left: 10px;
  font-size: 14px;
  line-height: 14px;
`

export default class File extends React.PureComponent {
  render() {
    return (
      <FileDiv isCurrent={this.props.isCurrent} onClick={this.props.onClick}>
        <Icon>attachment</Icon>
        <FileText>{this.props.name}</FileText>
      </FileDiv>
    )
  }
}

File.propTypes = {
  block: PropTypes.number.isRequired,
  tree: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  parent: PropTypes.number,
  onClick: PropTypes.func,
  isCurrent: PropTypes.bool,
};
