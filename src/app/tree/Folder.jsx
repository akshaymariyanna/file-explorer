import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

import TreeNode from "./Node";
import FolderActions, {ButtonPanel} from "../FolderActions";

const FolderDiv = styled.div`
  box-shadow: -1px -1px 2px #222;
  font-size: 14px;
  line-height: 14px;
  margin-bottom: 2px;
`

const FolderContent = styled.div`
  color: ${props => props.isCurrent ? "lightseagreen" : "lightcyan"};
  cursor: default;
  display: flex;
  margin-left: 5px;
  min-width: 200px;
  ${ButtonPanel} {
    visibility: hidden;
  }
  &:hover {
    ${ButtonPanel} {
      visibility: visible;
    }
  }
`

const Children = styled.div`
  margin-left: 20px;
  display: flex;
  flex-direction: column;
`

export const Icon = styled.span.attrs({
  className: "material-icons",
})`
  background-color: transparent;
  margin: auto;
`

const FolderName = styled.p`
  margin-left: 5px;
  flex: 1;
`

export default class Folder extends React.PureComponent {
  constructor(props, ...args) {
    super(props, ...args);

    this.state = {
      collapsed: !props.isCurrent,
    }
  }
  render() {
    return (
      <FolderDiv>
        <FolderContent isCurrent={this.props.isCurrent}>
          <Icon>folder</Icon>
          <FolderName onClick={this.onClick}>{this.props.name}</FolderName>
          <FolderActions
            addFolder={this.addFolder}
            addFile={this.addFile}
          />
        </FolderContent>
        {
          !this.state.collapsed && (
            <Children>
              {
                this.props.nodes.map((x) => (
                  <TreeNode block={x} tree={this.props.tree} key={`node-${x}`}/>
                ))
              }
            </Children>
          )
        }
      </FolderDiv>
    )
  }

  componentDidUpdate(prevProps) {
    if (!prevProps.isCurrent && this.props.isCurrent) {
      this.setState({
        collapsed: false,
      })
    }
  }

  onClick = () => {
    this.setState(state => ({
      collapsed: !state.collapsed,
    }));
    this.props.onClick();
  }

  addFolder = () => {
    this.setState({
      collapsed: false,
    })
    this.props.addNewFolder(this.props.block, this.props.tree, "New Folder");
  }

  addFile = () => {
    this.setState({
      collapsed: false,
    })
    this.props.addNewFile(this.props.block, this.props.tree, "New File");
  }
}

Folder.propTypes = {
  block: PropTypes.number.isRequired,
  tree: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  nodes: PropTypes.arrayOf(PropTypes.number),
  parent: PropTypes.number,
  onClick: PropTypes.func,
  isCurrent: PropTypes.bool,
  addNewFile: PropTypes.func,
  addNewFolder: PropTypes.func,
};
