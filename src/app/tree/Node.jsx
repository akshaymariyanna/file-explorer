import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import TreeFolder from "./Folder";
import TreeFile from "./File";
import Root from "./Root";
import Bin from "./Bin";

import {setCurrentFolder, setCurrentNode, addNewFolder,addNewFile} from "../../lib/reducers";

class TreeNode extends React.PureComponent {
  render() {
    const { block, tree, type, name, nodes, parent, special, isCurrent, addNewFolder, addNewFile } = this.props;
    if (special === "root") {
      return <Root tree={tree} nodes={nodes} goHome={this.folderClick}/>;
    } else if (special === "recycle") {
      return <Bin openRecycleBin={this.folderClick}/>
    } else {
      return type === 1 ? (
        <TreeFolder
          block={block}
          tree={tree}
          name={name}
          nodes={nodes}
          parent={parent}
          onClick={this.folderClick}
          isCurrent={isCurrent}
          addNewFolder={addNewFolder}
          addNewFile={addNewFile}
        />
      ) : (
        <TreeFile
          block={block}
          tree={tree}
          name={name}
          onClick={this.fileClick}
          isCurrent={isCurrent}
        />
      );
    }
  }

  folderClick = () => {
    this.props.setCurrentFolder(this.props.block, this.props.tree);
    this.props.setCurrentNode(this.props.block, this.props.tree);
  }

  fileClick = () => {
    this.props.setCurrentNode(this.props.block, this.props.tree);
  }
}

TreeNode.propTypes = {
  block: PropTypes.number.isRequired,
  tree: PropTypes.string.isRequired,
  type: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  nodes: PropTypes.arrayOf(PropTypes.number),
  parent: PropTypes.number,
  special: PropTypes.string,
  isCurrent: PropTypes.bool,
};

function mapStateToProps(store, ownProps) {
  const { type, name, nodes, parent } = store[ownProps.tree].blocks[ownProps.block];
  let isCurrent = false;
  if (type === 1) {
    isCurrent = store[ownProps.tree].currentFolder === ownProps.block;
  } else {
    isCurrent = store[ownProps.tree].currentNode === ownProps.block;
  }
  return {
    name,
    nodes,
    parent,
    type,
    isCurrent,
  }
}

const mapDispatchToProps = {
  setCurrentFolder,
  setCurrentNode,
  addNewFolder,
  addNewFile,
}

export default connect(mapStateToProps, mapDispatchToProps)(TreeNode);
