import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import {connect} from "react-redux";

import TreeNode from "./Node";
import FolderActions, {MyButton} from "../FolderActions"
import {switchTree} from "../../lib/store";

import { addNewFolder, addNewFile } from "../../lib/reducers";

const RootDiv = styled.div`
  padding: 5px;
  flex: 1 0;
  overflow-y: auto;
`

const RootPanel = styled.div`
  display: flex;
  justify-content: space-between;
`

const Children = styled.div`
  margin-top: 5px;
`

class Root extends React.PureComponent {
  render() {
    return (
      <RootDiv>
        <RootPanel>
          <MyButton onClick={this.props.goHome}>
            home
          </MyButton>
          <select value={this.props.tree} onChange={this.onTreeChange}>
            <option value="short">Short</option>
            <option value="long">Long</option>
          </select>
          <FolderActions
            addFolder={this.addFolder}
            addFile={this.addFile}
          />
        </RootPanel>
        <Children>
          {
            this.props.nodes.map((x) => (
              <TreeNode block={x} tree={this.props.tree} key={`node-${x}`}/>
            ))
          }
        </Children>
      </RootDiv>
    )
  }

  addFolder = () => {
    this.props.addNewFolder(0, this.props.tree, "New Folder");
  }

  addFile = () => {
    this.props.addNewFile(0, this.props.tree, "New File");
  }

  onTreeChange = (e) => {
    this.props.switchTree(e.target.value);
  }
}

Root.propTypes = {
  tree: PropTypes.string.isRequired,
  nodes: PropTypes.arrayOf(PropTypes.number),
};

export default connect(null, {
  addNewFolder,
  addNewFile,
  switchTree,
})(Root);
