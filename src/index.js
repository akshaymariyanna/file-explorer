import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from "react-redux";
import "./index.css";
import "material-design-icons/iconfont/material-icons.css";

import App from './app';
import configureStore from "./lib/store";
import registerServiceWorker from './registerServiceWorker';

require("typeface-open-sans")

const store = configureStore();

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>, document.getElementById('root'));
registerServiceWorker();
