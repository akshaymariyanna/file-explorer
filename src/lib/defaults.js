export default function getDefaults(which, persist = false) {
  const ts = (new Date()).toDateString();
  const blocks = [{
    type: 1,
    name: "root",
    nodes: [],
    parent: null,
    lastModified: ts,
  }, {
    type: 1,
    name: "recycle",
    nodes: [],
    parent: null,
    lastModified: ts,
  }];

  let doc = which === "short" ? short : long;

  for (let x of doc) {
    blocks[0].nodes.push(blocks.length);
    blocks.push({
      type: 1,
      name: x.title,
      nodes: [blocks.length + 1],
      parent: 0,
      lastModified: ts,
    }, {
      type: 1,
      name: "Game play resources",
      nodes: [blocks.length + 2, blocks.length + 3],
      parent: blocks.length,
      lastModified: ts,
    }, {
      type: 1,
      name: "Installation",
      nodes: [],
      parent: blocks.length + 1,
      lastModified: ts,
    }, {
      type: 1,
      name: "Resource Dependency",
      nodes: [],
      parent: blocks.length + 1,
      lastModified: ts,
    });

    const inst = blocks.length - 2;
    const depe = blocks.length - 1;

    for (let f of x["Game play resources"]["Installation"]) {
      blocks[inst].nodes.push(blocks.length);
      blocks.push({
        type: 0,
        name: f.file_name,
        format: f.type,
        parent: inst,
        lastModified: ts,
      })
    }
    for (let f of x["Game play resources"]["Resource Dependency"]) {
      blocks[depe].nodes.push(blocks.length);
      blocks.push({
        type: 0,
        name: f.file_name,
        format: f.type,
        parent: depe,
        lastModified: ts,
      })
    }
  }

  for (let b of blocks) {
    if (b.nodes) {
      b.nodes.sort((x, y) => {
        const xo = blocks[x];
        const yo = blocks[y];
        if (xo.type !== yo.type) {
          return yo.type - xo.type;
        }
        return xo.name < yo.name ? -1 : 1;
      })
    }
  }

  if (persist) {
      localStorage.setItem(which, JSON.stringify(blocks));
  }

  return blocks;
}

const short = [
    {
      "title": "LittleBigPlanet PS Vita",
      "Game play resources":
        {
          "Installation":
            [
              { "file_name": "install.iso", "type": "iso image" },
              { "file_name": "archive_unbox.dat", "type": "dat file" }
            ],
          "Resource Dependency":
              [
                { "file_name": "profile.sav", "type": "sav file" },
                { "file_name": "snd0.AT3", "type": "AT3 file" }
              ]
        }
    },
    {
      "title": "Marvel Super Hero Edition",
      "Game play resources":
        {
          "Installation":
            [
              { "file_name": "install.iso", "type": "iso image" },
              { "file_name": "archive_unbox.dat", "type": "dat file" }
            ],
          "Resource Dependency":
              [
                { "file_name": "profile.sav", "type": "sav file" },
                { "file_name": "snd0.AT3", "type": "AT3 file" }
              ]
        }
    },
    {
      "title": "Splice: Tree of Life",
      "Game play resources":
        {
          "Installation":
            [
              { "file_name": "install.iso", "type": "iso image" },
              { "file_name": "archive_unbox.dat", "type": "dat file" }
            ],
          "Resource Dependency":
              [
                { "file_name": "profile.sav", "type": "sav file" },
                { "file_name": "snd0.AT3", "type": "AT3 file" }
              ]
        }
    }
];

const long = [
    {
      "title": "LittleBigPlanet PS Vita",
      "Game play resources":
        {
          "Installation":
            [
              { "file_name": "install.iso", "type": "iso image" },
              { "file_name": "archive_unbox.dat", "type": "dat file" }
            ],
          "Resource Dependency":
              [
                { "file_name": "profile.sav", "type": "sav file" },
                { "file_name": "snd0.AT3", "type": "AT3 file" }
              ]
        }
    },
    {
      "title": "Marvel Super Hero Edition",
      "Game play resources":
        {
          "Installation":
            [
              { "file_name": "install.iso", "type": "iso image" },
              { "file_name": "archive_unbox.dat", "type": "dat file" }
            ],
          "Resource Dependency":
              [
                { "file_name": "profile.sav", "type": "sav file" },
                { "file_name": "snd0.AT3", "type": "AT3 file" }
              ]
        }
    },
    {
      "title": "Splice: Tree of Life",
      "Game play resources":
        {
          "Installation":
            [
              { "file_name": "install.iso", "type": "iso image" },
              { "file_name": "archive_unbox.dat", "type": "dat file" }
            ],
          "Resource Dependency":
              [
                { "file_name": "profile.sav", "type": "sav file" },
                { "file_name": "snd0.AT3", "type": "AT3 file" }
              ]
        }
    },
    {
      "title": "NHL 13",
      "Game play resources":
        {
          "Installation":
            [
              { "file_name": "install.iso", "type": "iso image" },
              { "file_name": "archive_unbox.dat", "type": "dat file" }
            ],
          "Resource Dependency":
              [
                { "file_name": "profile.sav", "type": "sav file" },
                { "file_name": "snd0.AT3", "type": "AT3 file" }
              ]
        }
    },
    {
      "title": "Total War Battles: Shogun",
      "Game play resources":
        {
          "Installation":
            [
              { "file_name": "install.iso", "type": "iso image" },
              { "file_name": "archive_unbox.dat", "type": "dat file" }
            ],
          "Resource Dependency":
              [
                { "file_name": "profile.sav", "type": "sav file" },
                { "file_name": "snd0.AT3", "type": "AT3 file" }
              ]
        }
    },
    {
      "title": "Double Dragon: Neon",
      "Game play resources":
        {
          "Installation":
            [
              { "file_name": "install.iso", "type": "iso image" },
              { "file_name": "archive_unbox.dat", "type": "dat file" }
            ],
          "Resource Dependency":
              [
                { "file_name": "profile.sav", "type": "sav file" },
                { "file_name": "snd0.AT3", "type": "AT3 file" }
              ]
        }
    },
    {
      "title": "Guild Wars 2",
      "Game play resources":
        {
          "Installation":
            [
              { "file_name": "install.iso", "type": "iso image" },
              { "file_name": "archive_unbox.dat", "type": "dat file" }
            ],
          "Resource Dependency":
              [
                { "file_name": "profile.sav", "type": "sav file" },
                { "file_name": "snd0.AT3", "type": "AT3 file" }
              ]
        }
    },
    {
      "title": "Tekken Tag Tournament 2",
      "Game play resources":
        {
          "Installation":
            [
              { "file_name": "install.iso", "type": "iso image" },
              { "file_name": "archive_unbox.dat", "type": "dat file" }
            ],
          "Resource Dependency":
              [
                { "file_name": "profile.sav", "type": "sav file" },
                { "file_name": "snd0.AT3", "type": "AT3 file" }
              ]
        }
    },
    {
      "title": "Tekken Tag Tournament 2",
      "Game play resources":
        {
          "Installation":
            [
              { "file_name": "install.iso", "type": "iso image" },
              { "file_name": "archive_unbox.dat", "type": "dat file" }
            ],
          "Resource Dependency":
              [
                { "file_name": "profile.sav", "type": "sav file" },
                { "file_name": "snd0.AT3", "type": "AT3 file" }
              ]
        }
    },
    {
      "title": "Wild Blood",
      "Game play resources":
        {
          "Installation":
            [
              { "file_name": "install.iso", "type": "iso image" },
              { "file_name": "archive_unbox.dat", "type": "dat file" }
            ],
          "Resource Dependency":
              [
                { "file_name": "profile.sav", "type": "sav file" },
                { "file_name": "snd0.AT3", "type": "AT3 file" }
              ]
        }
    },
    {
      "title": "Mark of the Ninja",
      "Game play resources":
        {
          "Installation":
            [
              { "file_name": "install.iso", "type": "iso image" },
              { "file_name": "archive_unbox.dat", "type": "dat file" }
            ],
          "Resource Dependency":
              [
                { "file_name": "profile.sav", "type": "sav file" },
                { "file_name": "snd0.AT3", "type": "AT3 file" }
              ]
        }
    },
    {
      "title": "Home: A Unique Horror Adventure",
      "Game play resources":
        {
          "Installation":
            [
              { "file_name": "install.iso", "type": "iso image" },
              { "file_name": "archive_unbox.dat", "type": "dat file" }
            ],
          "Resource Dependency":
              [
                { "file_name": "profile.sav", "type": "sav file" },
                { "file_name": "snd0.AT3", "type": "AT3 file" }
              ]
        }
    },
    {
      "title": "Avengers Initiative",
      "Game play resources":
        {
          "Installation":
            [
              { "file_name": "install.iso", "type": "iso image" },
              { "file_name": "archive_unbox.dat", "type": "dat file" }
            ],
          "Resource Dependency":
              [
                { "file_name": "profile.sav", "type": "sav file" },
                { "file_name": "snd0.AT3", "type": "AT3 file" }
              ]
        }
    },
    {
      "title": "Way of the Samurai 4",
      "Game play resources":
        {
          "Installation":
            [
              { "file_name": "install.iso", "type": "iso image" },
              { "file_name": "archive_unbox.dat", "type": "dat file" }
            ],
          "Resource Dependency":
              [
                { "file_name": "profile.sav", "type": "sav file" },
                { "file_name": "snd0.AT3", "type": "AT3 file" }
              ]
        }
    },
    {
      "title": "JoJo's Bizarre Adventure HD",
      "Game play resources":
        {
          "Installation":
            [
              { "file_name": "install.iso", "type": "iso image" },
              { "file_name": "archive_unbox.dat", "type": "dat file" }
            ],
          "Resource Dependency":
              [
                { "file_name": "profile.sav", "type": "sav file" },
                { "file_name": "snd0.AT3", "type": "AT3 file" }
              ]
        }
    },
    {
      "title": "Mass Effect 3: Leviathan",
      "Game play resources":
        {
          "Installation":
            [
              { "file_name": "install.iso", "type": "iso image" },
              { "file_name": "archive_unbox.dat", "type": "dat file" }
            ],
          "Resource Dependency":
              [
                { "file_name": "profile.sav", "type": "sav file" },
                { "file_name": "snd0.AT3", "type": "AT3 file" }
              ]
        }
    },
    {
      "title": "Dark Souls",
      "Game play resources":
        {
          "Installation":
            [
              { "file_name": "install.iso", "type": "iso image" },
              { "file_name": "archive_unbox.dat", "type": "dat file" }
            ],
          "Resource Dependency":
              [
                { "file_name": "profile.sav", "type": "sav file" },
                { "file_name": "snd0.AT3", "type": "AT3 file" }
              ]
        }
    },
    {
      "title": "Symphony",
      "Game play resources":
        {
          "Installation":
            [
              { "file_name": "install.iso", "type": "iso image" },
              { "file_name": "archive_unbox.dat", "type": "dat file" }
            ],
          "Resource Dependency":
              [
                { "file_name": "profile.sav", "type": "sav file" },
                { "file_name": "snd0.AT3", "type": "AT3 file" }
              ]
        }
    },
    {
      "title": "Bastion",
      "Game play resources":
        {
          "Installation":
            [
              { "file_name": "install.iso", "type": "iso image" },
              { "file_name": "archive_unbox.dat", "type": "dat file" }
            ],
          "Resource Dependency":
              [
                { "file_name": "profile.sav", "type": "sav file" },
                { "file_name": "snd0.AT3", "type": "AT3 file" }
              ]
        }
    },
    {
      "title": "Tom Clancy's Ghost Recon Phantoms",
      "Game play resources":
        {
          "Installation":
            [
              { "file_name": "install.iso", "type": "iso image" },
              { "file_name": "archive_unbox.dat", "type": "dat file" }
            ],
          "Resource Dependency":
              [
                { "file_name": "profile.sav", "type": "sav file" },
                { "file_name": "snd0.AT3", "type": "AT3 file" }
              ]
        }
    },
    {
      "title": "Thirty Flights of Loving",
      "Game play resources":
        {
          "Installation":
            [
              { "file_name": "install.iso", "type": "iso image" },
              { "file_name": "archive_unbox.dat", "type": "dat file" }
            ],
          "Resource Dependency":
              [
                { "file_name": "profile.sav", "type": "sav file" },
                { "file_name": "snd0.AT3", "type": "AT3 file" }
              ]
        }
    },
    {
      "title": "Legasista",
      "Game play resources":
        {
          "Installation":
            [
              { "file_name": "install.iso", "type": "iso image" },
              { "file_name": "archive_unbox.dat", "type": "dat file" }
            ],
          "Resource Dependency":
              [
                { "file_name": "profile.sav", "type": "sav file" },
                { "file_name": "snd0.AT3", "type": "AT3 file" }
              ]
        }
    },
    {
      "title": "The Walking Dead: The Game",
      "Game play resources":
        {
          "Installation":
            [
              { "file_name": "install.iso", "type": "iso image" },
              { "file_name": "archive_unbox.dat", "type": "dat file" }
            ],
          "Resource Dependency":
              [
                { "file_name": "profile.sav", "type": "sav file" },
                { "file_name": "snd0.AT3", "type": "AT3 file" }
              ]
        }
    },
    {
      "title": "World of Warcraft: Mists of Pandaria",
      "Game play resources":
        {
          "Installation":
            [
              { "file_name": "install.iso", "type": "iso image" },
              { "file_name": "archive_unbox.dat", "type": "dat file" }
            ],
          "Resource Dependency":
              [
                { "file_name": "profile.sav", "type": "sav file" },
                { "file_name": "snd0.AT3", "type": "AT3 file" }
              ]
        }
    },
    {
      "title": "Wrath of the Dead Rabbit",
      "Game play resources":
        {
          "Installation":
            [
              { "file_name": "install.iso", "type": "iso image" },
              { "file_name": "archive_unbox.dat", "type": "dat file" }
            ],
          "Resource Dependency":
              [
                { "file_name": "profile.sav", "type": "sav file" },
                { "file_name": "snd0.AT3", "type": "AT3 file" }
              ]
        }
    },
    {
      "title": "Pokemon White Version 2",
      "Game play resources":
        {
          "Installation":
            [
              { "file_name": "install.iso", "type": "iso image" },
              { "file_name": "archive_unbox.dat", "type": "dat file" }
            ],
          "Resource Dependency":
              [
                { "file_name": "profile.sav", "type": "sav file" },
                { "file_name": "snd0.AT3", "type": "AT3 file" }
              ]
        }
    },
    {
      "title": "War of the Roses",
      "Game play resources":
        {
          "Installation":
            [
              { "file_name": "install.iso", "type": "iso image" },
              { "file_name": "archive_unbox.dat", "type": "dat file" }
            ],
          "Resource Dependency":
              [
                { "file_name": "profile.sav", "type": "sav file" },
                { "file_name": "snd0.AT3", "type": "AT3 file" }
              ]
        }
    },
    {
      "title": "Pokemon Black Version 2",
      "Game play resources":
        {
          "Installation":
            [
              { "file_name": "install.iso", "type": "iso image" },
              { "file_name": "archive_unbox.dat", "type": "dat file" }
            ],
          "Resource Dependency":
              [
                { "file_name": "profile.sav", "type": "sav file" },
                { "file_name": "snd0.AT3", "type": "AT3 file" }
              ]
        }
    },
    {
      "title": "Drakerider",
      "Game play resources":
        {
          "Installation":
            [
              { "file_name": "install.iso", "type": "iso image" },
              { "file_name": "archive_unbox.dat", "type": "dat file" }
            ],
          "Resource Dependency":
              [
                { "file_name": "profile.sav", "type": "sav file" },
                { "file_name": "snd0.AT3", "type": "AT3 file" }
              ]
        }
    },
    {
      "title": "Rock Band Blitz",
      "Game play resources":
        {
          "Installation":
            [
              { "file_name": "install.iso", "type": "iso image" },
              { "file_name": "archive_unbox.dat", "type": "dat file" }
            ],
          "Resource Dependency":
              [
                { "file_name": "profile.sav", "type": "sav file" },
                { "file_name": "snd0.AT3", "type": "AT3 file" }
              ]
        }
    },
    {
      "title": "Counter-Strike: Global Offensive",
      "Game play resources":
        {
          "Installation":
            [
              { "file_name": "install.iso", "type": "iso image" },
              { "file_name": "archive_unbox.dat", "type": "dat file" }
            ],
          "Resource Dependency":
              [
                { "file_name": "profile.sav", "type": "sav file" },
                { "file_name": "snd0.AT3", "type": "AT3 file" }
              ]
        }
    },
    {
      "title": "Worms Revolution",
      "Game play resources":
        {
          "Installation":
            [
              { "file_name": "install.iso", "type": "iso image" },
              { "file_name": "archive_unbox.dat", "type": "dat file" }
            ],
          "Resource Dependency":
              [
                { "file_name": "profile.sav", "type": "sav file" },
                { "file_name": "snd0.AT3", "type": "AT3 file" }
              ]
        }
    },
    {
      "title": "Bad Piggies",
      "Game play resources":
        {
          "Installation":
            [
              { "file_name": "install.iso", "type": "iso image" },
              { "file_name": "archive_unbox.dat", "type": "dat file" }
            ],
          "Resource Dependency":
              [
                { "file_name": "profile.sav", "type": "sav file" },
                { "file_name": "snd0.AT3", "type": "AT3 file" }
              ]
        }
    },
    {
      "title": "Resident Evil 6",
      "Game play resources":
        {
          "Installation":
            [
              { "file_name": "install.iso", "type": "iso image" },
              { "file_name": "archive_unbox.dat", "type": "dat file" }
            ],
          "Resource Dependency":
              [
                { "file_name": "profile.sav", "type": "sav file" },
                { "file_name": "snd0.AT3", "type": "AT3 file" }
              ]
        }
    },
    {
      "title": "Shad O",
      "Game play resources":
        {
          "Installation":
            [
              { "file_name": "install.iso", "type": "iso image" },
              { "file_name": "archive_unbox.dat", "type": "dat file" }
            ],
          "Resource Dependency":
              [
                { "file_name": "profile.sav", "type": "sav file" },
                { "file_name": "snd0.AT3", "type": "AT3 file" }
              ]
        }
    },
    {
      "title": "Demons Score",
      "Game play resources":
        {
          "Installation":
            [
              { "file_name": "install.iso", "type": "iso image" },
              { "file_name": "archive_unbox.dat", "type": "dat file" }
            ],
          "Resource Dependency":
              [
                { "file_name": "profile.sav", "type": "sav file" },
                { "file_name": "snd0.AT3", "type": "AT3 file" }
              ]
        }
    },
    {
      "title": "NBA 2K13",
      "Game play resources":
        {
          "Installation":
            [
              { "file_name": "install.iso", "type": "iso image" },
              { "file_name": "archive_unbox.dat", "type": "dat file" }
            ],
          "Resource Dependency":
              [
                { "file_name": "profile.sav", "type": "sav file" },
                { "file_name": "snd0.AT3", "type": "AT3 file" }
              ]
        }
    },
    {
      "title": "The World Ends with You: Solo Remix",
      "Game play resources":
        {
          "Installation":
            [
              { "file_name": "install.iso", "type": "iso image" },
              { "file_name": "archive_unbox.dat", "type": "dat file" }
            ],
          "Resource Dependency":
              [
                { "file_name": "profile.sav", "type": "sav file" },
                { "file_name": "snd0.AT3", "type": "AT3 file" }
              ]
        }
    },
    {
      "title": "Madden NFL 13",
      "Game play resources":
        {
          "Installation":
            [
              { "file_name": "install.iso", "type": "iso image" },
              { "file_name": "archive_unbox.dat", "type": "dat file" }
            ],
          "Resource Dependency":
              [
                { "file_name": "profile.sav", "type": "sav file" },
                { "file_name": "snd0.AT3", "type": "AT3 file" }
              ]
        }
    },
    {
      "title": "Lili: Child of Geoss",
      "Game play resources":
        {
          "Installation":
            [
              { "file_name": "install.iso", "type": "iso image" },
              { "file_name": "archive_unbox.dat", "type": "dat file" }
            ],
          "Resource Dependency":
              [
                { "file_name": "profile.sav", "type": "sav file" },
                { "file_name": "snd0.AT3", "type": "AT3 file" }
              ]
        }
    }
];
