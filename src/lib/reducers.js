export const setCurrentFolder = (folder, tree) => ({ type: "SET_CURRENT_FOLDER", tree, folder });
export const setCurrentNode = (node, tree) => ({ type: "SET_CURRENT_NODE", tree, node });
export const addNewFolder = (parent, tree, name) => ({ type: "ADD_FOLDER", tree, parent, name });
export const addNewFile = (parent, tree, name) => ({ type: "ADD_FILE", tree, parent, name });
export const deleteNode = (tree, node) => ({ type: "DELETE_NODE", node, tree });
export const restoreNode = (tree, node) => ({ type: "RESTORE_NODE", node, tree });
export const cleanBin = (tree) => ({ type: "CLEAN_BIN", tree });

export default function fs(loyal){
  return (state = {}, action) => {
    if (action.tree !== loyal) {
      return state;
    }

    switch (action.type) {
      case "SET_CURRENT_FOLDER":
      if (state.blocks[action.folder]) {
        return {...state, currentFolder: action.folder};
      } else {
        return state;
      }
      case "SET_CURRENT_NODE":
      if (state.blocks[action.node]) {
        return {...state, currentNode: action.node};
      } else {
        return state;
      }
      case "CLEAN_BIN":
      {
        if (action.node === 0 || action.node === 1) {
          return state;
        }
        if (state.blocks[1].nodes.length <= 0) {
          return state;
        }
        const newState = {...state, blocks: [...state.blocks]};
        newState.blocks[1] = {...newState.blocks[1], nodes: []};

        return newState;
      }
      case "RESTORE_NODE":
      {
        if (action.node === 0 || action.node === 1) {
          return state;
        }
        const currParent = state.blocks[action.node].parent;
        if (
          state.blocks[1].nodes.includes(currParent) ||
          state.blocks[currParent].nodes.some((x) => state.blocks[x].name === state.blocks[action.node].name)
        ) {
          return state;
        }
        const newState = {...state, blocks: [...state.blocks]};
        newState.blocks[1] = {...newState.blocks[1]};
        newState.blocks[currParent] = {...newState.blocks[currParent], nodes: [...newState.blocks[currParent].nodes, action.node]};
        newState.blocks[1].nodes = newState.blocks[1].nodes.filter((x) => x !== action.node);

        newState.blocks[currParent].nodes.sort((x, y) => {
          const xo = newState.blocks[x];
          const yo = newState.blocks[y];
          if (xo.type !== yo.type) {
            return yo.type - xo.type;
          }
          return xo.name < yo.name ? -1 : 1;
        });

        return newState;
      }
      case "DELETE_NODE":
      {
        if (action.node === 0 || action.node === 1) {
          return state;
        }
        const currParent = state.blocks[action.node].parent;
        const newState = {...state, blocks: [...state.blocks], currentNode: currParent};
        newState.blocks[1] = {...newState.blocks[1], nodes: [...newState.blocks[1].nodes, action.node]};
        newState.blocks[currParent] = {...newState.blocks[currParent]};
        newState.blocks[currParent].nodes = newState.blocks[currParent].nodes.filter((x) => x !== action.node);

        newState.blocks[1].nodes.sort((x, y) => {
          const xo = newState.blocks[x];
          const yo = newState.blocks[y];
          if (xo.type !== yo.type) {
            return yo.type - xo.type;
          }
          return xo.name < yo.name ? -1 : 1;
        });

        return newState;
      }
      case "ADD_FOLDER":
      case "ADD_FILE":
      if (state.blocks[action.parent]) {
        const newState = {...state, blocks: [...state.blocks]};
        newState.blocks[action.parent] = {...newState.blocks[action.parent]};
        newState.blocks[action.parent].nodes = [...newState.blocks[action.parent].nodes];
        const nodes = newState.blocks[action.parent].nodes;
        const newNode = {
          type: action.type === "ADD_FILE" ? 0 : 1,
          nodes: action.type === "ADD_FILE" ? null : [],
          parent: action.parent,
          lastModified: (new Date()).toDateString(),
          name: action.name,
          format: action.type === "ADD_FILE" ? "Text Document" : null,
        };
        let i = 1;
        while (nodes.some((x) => newState.blocks[x].name === newNode.name)) {
          newNode.name = `${action.name}_${i++}`;
        }
        nodes.push(newState.blocks.length);
        newState.blocks.push(newNode);

        nodes.sort((x, y) => {
          const xo = newState.blocks[x];
          const yo = newState.blocks[y];
          if (xo.type !== yo.type) {
            return yo.type - xo.type;
          }
          return xo.name < yo.name ? -1 : 1;
        });

        return newState;
      } else {
        return state;
      }
      default:
      return state;
    }
  }
}
