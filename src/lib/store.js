import { createStore, combineReducers } from "redux";

import getDefaults from "./defaults";
import fs from "./reducers";

const lShort = localStorage.getItem("short");
const lLong = localStorage.getItem("long");

const short = {
  blocks: lShort ? JSON.parse(lShort) : getDefaults("short", true),
  root: 0,
  recycle: 1,
  currentFolder: 0,
  currentNode: 0,
  clipboard: null,
};
const long = {
  blocks: lLong ? JSON.parse(lLong) : getDefaults("long", true),
  root: 0,
  recycle: 1,
  currentFolder: 0,
  currentNode: 0,
  clipboard: null,
}


export const switchTree = (tree) => ({ type: "SWICTH_TREE", tree });

function changeCurrentTree(state = "short", action) {
  if (action.type === "SWICTH_TREE") {
    return action.tree === "long" ? "long" : "short";
  } else {
    return state;
  }
}

export default function configureStore() {
  return createStore(combineReducers({
    short: fs("short"),
    long: fs("long"),
    current: changeCurrentTree,
  }), {
    short,
    long,
    current: "short",
  });
}
